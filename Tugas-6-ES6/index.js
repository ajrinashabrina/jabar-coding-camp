
// Soal 1
const MenghitungLuasPersegi = (p,l) => { 
    return p*l; }
console.log(MenghitungLuasPersegi(3,2)) 

const MenghitungKelilingPersegi = (p,l) => { 
    return 2*p+2*l; }
console.log(MenghitungKelilingPersegi(3,2)) 

// Soal 2
const newFunction = (firstName,lastName) => { 
    return firstName, lastName;
}
console.log(newFunction("William", "Imoh"))

// Soal 3
const newObject = {
firstName: "Muhammad",
lastName: "Iqbal Mubarok",
address: "Jalan Ranamanyar",
hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log (firstName, lastName, address, hobby)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)

// Soal 5
const planet = "earth"
const view = "glass"

const theString  = `${planet} ${view}`

console.log(theString)

 


